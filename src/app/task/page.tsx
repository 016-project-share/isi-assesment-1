'use client'

import React, { useCallback, useEffect, useState } from 'react';
import Header from '@/components/header';
import TaskComp from './components/task';
import { TTaskData, useAuth } from '@/context/authContext';
import { fetchApi } from '@/utils/fetchApi';

const TaskPage = () => {
   const { user, setTaskList } = useAuth();
   const [selectedTask, setSelectedTask] = useState<TTaskData|null>(null);

   const [isLoading, setIsLoading] = useState(false);
   
   const getData = useCallback(async (withLoading:boolean=false) => {
      if(user?.id){
         withLoading && setIsLoading(true);
         let {resp: { data=[] }, resp_status = "NOK"} = await fetchApi({
            path: `api/todos?userId=${user?.id}`,
            method: "GET",
         });

         if(data.length > 0){
            setTaskList(data);
         }
         withLoading && setIsLoading(false);
      }
   },[user?.id, setTaskList])
   
   useEffect(() => {
      getData(true);
   },[]);

   const doAction = async () => {
      let { id, title, completed } = selectedTask ?? {};
      let path = 'api/todos', method = 'POST';
      if(id){
         path = `api/todos?id=${id}`;
         method = 'PUT'
      }
      let {resp: { data=[] }, resp_status = "NOK"} = await fetchApi({
         path,  method,
         body: { title, completed: completed || false, userId: user?.id }
      });
      setSelectedTask(null);
      getData();
   }

   return (<>
      <Header />
      <div className=''>
         <div className='flex flex-col justify-center items-center w-[580px] min-h-[calc(100vh_-_80px)] mx-auto gap-y-4'>
            <div className='text-5xl'>Task Management</div>
            <div className='items-start self-start w-full'>
               <label htmlFor='title' className='w-full'>Title</label><br/>
               <input type='text' name='title' className='border border-black rounded-[10px] w-full h-12 p-5' 
                  value={selectedTask?.title ?? ""}
                  onChange={e => setSelectedTask(s => ({ ...s, title: e.target.value}) )}
                  onFocus={e => e.target.select()}
               />
            </div>
            
            <div className='flex justify-center items-center gap-x-2'>
               <button 
                  className={`py-3 px-5 rounded-[10px] ${selectedTask?.id ? 'bg-[#FFB46F]' : 'bg-[#6FCBFF]' }`} 
                  onClick={doAction}
               >
               { selectedTask?.id ? "Update Task" : "Add Task" }
               </button>
               {selectedTask?.title && 
                  <button className='bg-[#FF6F6F] py-3 px-5 rounded-[10px]' onClick={() => setSelectedTask(null)}>
                     Cancel
                  </button>
               }
            </div>
            { isLoading ?
               <p>Pelase wait ...</p> 
            : 
               <TaskComp setTask={setSelectedTask} getData={getData} /> 
            }

         </div>
      </div>
   </>)
}

export default TaskPage