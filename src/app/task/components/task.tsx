'use client'

import React, { useMemo } from 'react';

import { TTaskData, useAuth } from '@/context/authContext';
import { fetchApi } from '@/utils/fetchApi';
import TaskItem from './taskItem';
import NoTask from './no-task';

export type TAction = "edit"|"switch"|"delete";

type TTaskComp = {
   setTask: (d:TTaskData) => void,
   getData: () => void;
}

const TaskComp = ({ setTask, getData }: TTaskComp) => {
   const { task=[] } = useAuth();

   const doAction = async (act:TAction,d:TTaskData) => {
      if(act === 'edit'){
         setTask(d);
      } else if(act === "switch"){
         let {resp: { data=[] }, resp_status = "NOK"} = await fetchApi({
            path: `api/todos?id=${d.id}`,
            method: "PUT",
            body: { ...d, completed: !d.completed }
         });
      } else {
         let {resp: { data=[] }, resp_status = "NOK"} = await fetchApi({
            path: `api/todos?id=${d.id}`,
            method: "DELETE"
         });
      }
      act !== 'edit' && getData();
   }

   const completedtask = useMemo(() => task.filter(d => d.completed),[task]);
   const ongoingtask = useMemo(() => task.filter(d => !d.completed),[task]);

   if(task.length === 0){
      return <NoTask />
   }

   return (<>
      <div className="self-start w-full">
         <div className='text-base font-bold'>Ongoing Task</div>
         <div className='flex flex-col gap-y-2'>
            {ongoingtask && ongoingtask.map(d => (
               <TaskItem 
                  { ...d} 
                  key={"ongoing"+d.id} 
                  doAction={(act) => doAction(act, d)}
               />
            ))}
         </div>
      </div>

      <div className="self-start w-full">
         <div className='text-base font-bold'>Completed Task</div>
         <div className='flex flex-col gap-y-2'>
            {completedtask && completedtask.map(d => (
               <TaskItem 
                  { ...d}
                  key={"completed"+d.id} 
                  doAction={(act) => doAction(act, d)}
               />
            ))}
         </div>
      </div>
   </>)
}

export default TaskComp