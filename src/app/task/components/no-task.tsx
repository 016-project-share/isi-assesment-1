import React from 'react'

const NoTask = () => {
   return (
      <div className='flex justify-between items-center bg-[#D0D0D0] p-3 pb-2 w-full rounded-[10px]'>
         <p>You don&lsquo;t have a task. Please create a task.</p>
      </div>
   )
}

export default NoTask