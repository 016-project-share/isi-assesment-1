'use client'

import React, { useEffect } from 'react';
import { useRouter } from 'next/navigation';

import { useAuth } from '@/context/authContext';
import Header from '@/components/header';

const ProfilePage = () => {
   const { user, loading } = useAuth();
   const router = useRouter();

   useEffect(() => {
      if(user?.role === "GUEST"){
         router.push('/task');
      }
   },[user, loading, router]);

   return (<>
      <Header />
      <div className=''>
         <div className='flex flex-col justify-center items-center w-[580px] min-h-[calc(100vh_-_80px)] mx-auto gap-y-4'>
            <div className=' text-3xl'>
               email: {user?.email}<br/>
               fullname: {user?.name}
            </div>
         </div>
      </div>
   </>)
}

export default ProfilePage