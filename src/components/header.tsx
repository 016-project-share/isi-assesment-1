'use client'

import React from 'react';
import Link from 'next/link';
import { useAuth } from '@/context/authContext';

const Header = () => {
   const { user } = useAuth();

   return (
      <div className='flex justify-center items-center h-20 font-[400] gap-x-5 border-b-2 border-b-black text-[32px] text-black'>
         <div className='flex-auto text-center'>
            <Link href='/task' className='mx-2'>Task</Link>
            { user?.role === "USER" &&
               <Link href='/profile' className='mx-2'>Profile</Link>
            }
         </div>
         <div className='w-32 text-lg'>
            { user?.role  === "USER" ? `Hello ${user?.name}` : "Hello Guest" }
         </div>
      </div>
   )
}

export default Header