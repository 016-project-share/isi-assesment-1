import { getToken } from "next-auth/jwt";
import { NextResponse } from "next/server";
import type { NextRequest } from "next/server";

export async function middleware(req: NextRequest){
   
   // let cookie = req.cookies.get('next-auth.csrf-token');
   const token = await getToken({ req, secret: process.env.AUTH_SECRET, raw: true });

   if(!token){
      return NextResponse.redirect(new URL('/', req.url));
   }

   return NextResponse.next();
}

export const config = {
   matcher: ['/task', '/profile'],
};