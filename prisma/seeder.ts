import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

async function main() {
   await prisma.users.createMany({
      data: [
         {
            email: 'guest@example.com',
            password: 'password',
            fullname: 'User One',
            role: 'GUEST',
         },{
            email: 'user1@example.com',
            password: 'password',
            fullname: 'User One',
            role: 'USER',
         },
      ]
   });

   await prisma.todos.createMany({
      data: [
         {
            title: 'Task One',
            completed: false,
            userId: 1,
         }, {
            title: 'Task Two',
            completed: true,
            userId: 1,
         }, {
            title: 'Task One',
            completed: false,
            userId: 2,
         }, {
            title: 'Task Two',
            completed: true,
            userId: 2,
         },
      ]
   });

   console.log("create Seeder");
}

main()
   .catch((e) => {
      console.error(e);
      process.exit();
   })
   .finally(async () => {
      await prisma.$disconnect();
   })